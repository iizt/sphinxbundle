<?php

namespace IIZT\SphinxBundle\Pagerfanta;

use IIZT\SphinxBundle\Api\Query;
use IIZT\SphinxBundle\Api\Client;
use IIZT\SphinxBundle\Api\Exception\NoResultException;

use Pagerfanta\Adapter\AdapterInterface;

class SphinxAdapter implements AdapterInterface
{
    /**
     * @var IIZT\SphinxBundle\Api\Query
     */
    protected $query;
    protected $result;
    protected $closure;

    public function __construct(Query $query, $closure)
    {
        if (!$closure instanceof \Closure) {
            throw new Exception();
        }
        $this->closure = $closure;

        $this->query = $query;
        $this->result = false;
    }

    public function getNbResults()
    {
        return $this->getResult()->getTotalFound();
    }

    public function getSlice($offset, $limit)
    {
        $closure = $this->closure;

        $this->query->setOffset($offset);
        $this->query->setLimit($limit);

        $result = $this->getResult();
        $entityIDs = array();
        foreach ($result->getMatches() as $id => $match) {
            $entityIDs[] = $id;
        }

        if (empty($entityIDs)) {
            return array();
        }

        return $closure($entityIDs);
    }

    /**
     * @return \IIZT\SphinxBundle\Api\Result
     */
    private function getResult($force = false)
    {
        if (false === $this->result || $force) {
            $this->result = $this->query->getResult();
            if (false == $this->result->isCorrect()) {
                // Log reason of error
            }
        }

        return $this->result;
    }
}