<?php

namespace IIZT\SphinxBundle\Api;

class Query {
    private $sphinxClient;
    private $limit = 10;
    private $offset = 0;
    private $queryString = null;

    public function __construct(Client $sphinxClient)
    {
        $this->sphinxClient = $sphinxClient;
    }

    public function setQueryString($queryString)
    {
        $this->queryString = $queryString;
        return $this;
    }

    public function setLimit($limit)
    {
        $this->limit = $limit;
        return $this;
    }

    public function setOffset($offset)
    {
        $this->offset = $offset;
        return $this;
    }

    /**
     * @return \IIZT\SphinxBundle\Api\Result
     */
    public function getResult()
    {
        $this->sphinxClient->setLimits($this->offset, $this->limit);
        $this->sphinxClient->addQuery($this->queryString);

        return new Result($this->sphinxClient->runQueries());
    }
}