<?php

namespace IIZT\SphinxBundle\Api;

class Client
{
    /**
     * @var \SphinxClient
     */
    protected $sphinxClient;

    public function __construct($host, $port)
    {
        $this->sphinxClient = new \SphinxClient();
        $this->sphinxClient->SetServer((string)$host, (int)$port);
    }

    public function createQuery()
    {
        return new Query($this);
    }

    public function addQuery($query, $index ="*", $comment = "")
    {
        return $this->sphinxClient->AddQuery($query, $index, $comment);
    }

    public function setLimits($offset, $limit)
    {
        $this->sphinxClient->SetLimits($offset, $limit);
    }

    public function runQueries()
    {
        return $this->sphinxClient->RunQueries();
    }
}