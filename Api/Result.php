<?php

namespace IIZT\SphinxBundle\Api;

class Result {
    /**
     * Maximum amount of matches that the daemon keeps in
     * RAM for each index and can return to the client.
     */
    const MAX_MATCHES = 1000;

    private $rawResult;

    public function __construct($rawResult)
    {
        $this->rawResult = $rawResult;
    }

    public function isCorrect()
    {
        if (!is_array($this->rawResult)) {
            return false;
        }

        if ($this->rawResult[0]["status"] == SEARCHD_ERROR) {
            return false;
        }

        return true;
    }

    public function getTotalFound()
    {
        if (array_key_exists('total_found', $this->rawResult[0])) {
            return min(array(self::MAX_MATCHES, $this->rawResult[0]['total_found']));
        }
        return 0;
    }

    public function getTotal()
    {
        if (array_key_exists('total', $this->rawResult[0])) {
            return $this->rawResult[0]['total'];
        }
        return 0;
    }

    public function getMatches()
    {
        if (array_key_exists('matches', $this->rawResult[0])) {
            return $this->rawResult[0]['matches'];
        }
        return array();
    }
}