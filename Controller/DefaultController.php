<?php

namespace IIZT\SphinxBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class DefaultController extends Controller
{
    
    public function indexAction($name)
    {
        return $this->render('IIZTSphinxBundle:Default:index.html.twig', array('name' => $name));
    }
}
